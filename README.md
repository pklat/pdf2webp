This is a snall bash script that converts pdf to image files.
Its meant to be used when pdf pages were created as slices of scanned .jpg
(so that each page has several rows)

Unfortunately, unless the rows have height divisible by 8 or 16 pixels, 
we cannot losslessly combine them into single page.jpg

So this script combines them to one .png per page and converts it to webp.
There is an option to use knusperli for deblocking, it provides better results.

Requirements:

 * pdfimages  ( to extract jpg from pdf )
 * cwebp  ( optional, to convert to webp ) 
 * knusperli is optional, but reccommended for deblocking
 

Usage:


create these directories:
*  tmp/   ( slices in original .jpg and converted .png)
*  out/   ( combined .png)
*  out2/  ( converted to .webp )

then run:

 pdf2webp    input.pdf  parts_per_page resolution


where parts_per_page is number of rows/slices
and resolution is (first) row height.

Example:

 pdf2webp  file.pdf    3   500




